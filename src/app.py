import time
import pyautogui
from dataclasses import dataclass


@dataclass
class Pos:
    X: int
    Y: int


@dataclass
class Items:
    LEVEL_1 = Pos(1163, 505)
    LEVEL_2 = Pos(1375, 438)
    LEVEL_3 = Pos(1491, 550)
    START = Pos(1800, 787)
    ATTACK_1 = Pos(1674, 780)


def main():
    items = Items()
    while True:
        pyautogui.click(items.LEVEL_1.X, items.LEVEL_1.Y)
        time.sleep(2)
        pyautogui.click(items.START.X, items.START.Y)
        time.sleep(5)

        while pyautogui.pixel(items.ATTACK_1.X, items.ATTACK_1.Y)[0] != 0:
            pyautogui.click(items.ATTACK_1.X, items.ATTACK_1.Y)
            time.sleep(2)
        time.sleep(1)
